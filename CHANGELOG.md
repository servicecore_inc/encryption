# Changelog

All notable changes to this project will be documented in this file, in reverse chronological order by release.

## 1.1.0 - 2023-04-18

### Added

- Encryptor delegator/interface/trait for ease of use
