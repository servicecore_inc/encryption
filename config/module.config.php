<?php

use ServiceCore\Encryption\Algorithm\Algorithm;
use ServiceCore\Encryption\Algorithm\Factory\FFX as FFXEncryptionFactory;
use ServiceCore\Encryption\Algorithm\FFX as FFXEncryption;
use ServiceCore\Encryption\ViewHelper\Algorithm as AlgorithmViewHelper;
use ServiceCore\Encryption\ViewHelper\Factory\Algorithm as AlgorithmViewHelperFactory;

return [
    'service_manager' => [
        'factories' => [
            FFXEncryption::class => FFXEncryptionFactory::class,
        ],
        'aliases' => [
            Algorithm::class => FFXEncryption::class
        ]
    ],
    'view_helpers'    => [
        'factories' => [
            AlgorithmViewHelper::class => AlgorithmViewHelperFactory::class
        ]
    ],
];
