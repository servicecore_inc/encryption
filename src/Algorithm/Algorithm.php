<?php

namespace ServiceCore\Encryption\Algorithm;

interface Algorithm
{
    public function encrypt(string $string);
    public function decrypt(string $encryptedString);
}
