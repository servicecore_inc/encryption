<?php

namespace ServiceCore\Encryption\Algorithm;

use Janv\FFXRadix\FFXRadix;

class FFX implements Algorithm
{
    private const RADIX          = 62;
    private const MINIMUM_LENGTH = 5;

    private FFXRadix $ffx;
    private string   $key;
    private string   $tweak;

    public function __construct(string $key, string $tweak)
    {
        $this->key   = \hex2bin($key);
        $this->tweak = \hex2bin($tweak);
        $this->ffx   = new FFXRadix();
    }

    public function encrypt(string $string): string
    {
        $length = \strlen($string);

        if ($length < self::MINIMUM_LENGTH) {
            $string = \str_pad($string, self::MINIMUM_LENGTH, 0, \STR_PAD_LEFT);
        }

        return $this->ffx->encrypt($string, self::RADIX, $this->key, $this->tweak);
    }

    public function decrypt(string $encryptedString): string
    {
        $decrypted = $this->ffx->decrypt($encryptedString, self::RADIX, $this->key, $this->tweak);
        $length    = \strlen($decrypted);

        if ($length === self::MINIMUM_LENGTH) {
            $string = \ltrim($decrypted, '0');
        } else {
            $string = $decrypted;
        }

        return $string;
    }
}
