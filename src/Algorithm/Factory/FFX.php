<?php

namespace ServiceCore\Encryption\Algorithm\Factory;

use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Factory\FactoryInterface;
use RuntimeException;
use ServiceCore\Encryption\Algorithm\FFX as FFXContext;

class FFX implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, ?array $options = null): FFXContext
    {
        $encryptionConfig = $container->get('config')['encryption'] ?? null;

        if ($encryptionConfig === null) {
            throw new RuntimeException('Could not find `encryption` key in config');
        }

        $ffxConfig = $encryptionConfig['ffx'] ?? null;

        if ($ffxConfig === null) {
            throw new RuntimeException('Could not find `ffx` key in `encryption` config');
        }

        if (!\array_key_exists('key', $ffxConfig) || !\array_key_exists('tweak', $ffxConfig)
            || !\is_string($ffxConfig['key']) || !\is_string($ffxConfig['tweak'])) {
            throw new RuntimeException('Invalid tweak/key in hash config. Must be a string.');
        }

        return new FFXContext($ffxConfig['key'], $ffxConfig['tweak']);
    }
}
