<?php

namespace ServiceCore\Encryption\Delegator;

use InvalidArgumentException;
use Laminas\ServiceManager\Factory\DelegatorFactoryInterface;
use Psr\Container\ContainerInterface;
use ServiceCore\Encryption\Algorithm\Algorithm;
use ServiceCore\Encryption\Helper\EncryptorAwareInterface;

class EncryptorDelegator implements DelegatorFactoryInterface
{
    public function __invoke(
        ContainerInterface $container,
        $name,
        callable $callback,
        ?array $options = null
    ): EncryptorAwareInterface {
        $class = $callback();

        if (!$class instanceof EncryptorAwareInterface) {
            throw new InvalidArgumentException(
                \sprintf(
                    'Class %s does not implement %s',
                    \get_class($class),
                    EncryptorAwareInterface::class
                )
            );
        }

        $class->setEncryptor($container->get(Algorithm::class));

        return $class;
    }
}
