<?php

namespace ServiceCore\Encryption\Helper;

use InvalidArgumentException;
use ServiceCore\Encryption\Algorithm\Algorithm;

trait EncryptorAware
{
    private Algorithm $algorithm;

    public function getEncryptor(): Algorithm
    {
        if (!$this->algorithm) {
            throw new InvalidArgumentException('Algorithm is not set. Hint: use a delegator factory');
        }

        return $this->algorithm;
    }

    public function setEncryptor(Algorithm $algorithm): self
    {
        $this->algorithm = $algorithm;

        return $this;
    }
}
