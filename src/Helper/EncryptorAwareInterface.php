<?php

namespace ServiceCore\Encryption\Helper;

use ServiceCore\Encryption\Algorithm\Algorithm;

interface EncryptorAwareInterface
{
    public function getEncryptor(): Algorithm;
    public function setEncryptor(Algorithm $algorithm): self;
}
