<?php

namespace ServiceCore\Encryption\ViewHelper;

use Laminas\View\Helper\AbstractHelper;
use ServiceCore\Encryption\Algorithm\Algorithm as AlgorithmContext;

class Algorithm extends AbstractHelper
{
    private AlgorithmContext $context;

    public function __construct(AlgorithmContext $algorithm)
    {
        $this->context = $algorithm;
    }

    public function __invoke(string $string): string
    {
        return $this->context->encrypt($string);
    }
}
