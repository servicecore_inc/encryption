<?php

namespace ServiceCore\Encryption\ViewHelper\Factory;

use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Factory\FactoryInterface;
use ServiceCore\Encryption\Algorithm\FFX;
use ServiceCore\Encryption\ViewHelper\Algorithm as AlgorithmViewHelper;

class Algorithm implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, ?array $options = null): AlgorithmViewHelper
    {
        /** @var FFX $algo */
        $algo = $container->get(FFX::class);

        return new AlgorithmViewHelper($algo);
    }
}
