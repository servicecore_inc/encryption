<?php

namespace ServiceCore\Encryption\Test\Delegator;

use Laminas\ServiceManager\ServiceManager;
use PHPUnit\Framework\TestCase;
use ServiceCore\Encryption\Algorithm\Algorithm;
use ServiceCore\Encryption\Delegator\EncryptorDelegator;
use ServiceCore\Encryption\Helper\EncryptorAware;
use ServiceCore\Encryption\Helper\EncryptorAwareInterface;

class EncryptorDelegatorTest extends TestCase
{
    public function testInvokeAddsEncryptor(): void
    {
        $mockClass = new class implements EncryptorAwareInterface {
            use EncryptorAware;
        };

        $container = $this->createConfiguredMock(ServiceManager::class, [
            'get' => $this->createStub(Algorithm::class)
        ]);

        $delegator = new EncryptorDelegator();

        $result = $delegator($container, EncryptorAwareInterface::class, fn() => $mockClass);

        $this->assertInstanceOf(Algorithm::class, $result->getEncryptor());
    }

    public function testInvokeThrowsErrorIfMissingInterface(): void
    {
        $mockClass = new class {
            use EncryptorAware;
        };

        $container = $this->createConfiguredMock(ServiceManager::class, [
            'get' => $this->createStub(Algorithm::class)
        ]);

        $delegator = new EncryptorDelegator();

        $this->expectExceptionMessage(
            \sprintf('Class %s does not implement %s', \get_class($mockClass), EncryptorAwareInterface::class)
        );

        $delegator($container, EncryptorAwareInterface::class, fn() => $mockClass);
    }
}
