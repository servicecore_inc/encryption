<?php

namespace ServiceCore\Encryption\Test;

use PHPUnit\Framework\TestCase;
use ServiceCore\Encryption\Algorithm\Factory\FFX as FFXFactory;
use ServiceCore\Encryption\Algorithm\FFX;
use ServiceCore\Encryption\Module;
use ServiceCore\Encryption\ViewHelper\Algorithm;
use ServiceCore\Encryption\ViewHelper\Factory\Algorithm as AlgorithmFactory;

class ModuleTest extends TestCase
{
    public function testGetConfig(): void
    {
        $module = new Module();
        $config = $module->getConfig();

        $this->assertArrayHasKey('service_manager', $config);
        $this->assertIsArray($config['service_manager']);
        $this->assertArrayHasKey('factories', $config['service_manager']);
        $this->assertArrayHasKey(FFX::class, $config['service_manager']['factories']);
        $this->assertEquals(FFXFactory::class, $config['service_manager']['factories'][FFX::class]);

        $this->assertArrayHasKey('view_helpers', $config);
        $this->assertIsArray($config['view_helpers']);
        $this->assertArrayHasKey('factories', $config['view_helpers']);
        $this->assertArrayHasKey(Algorithm::class, $config['view_helpers']['factories']);
        $this->assertEquals(AlgorithmFactory::class, $config['view_helpers']['factories'][Algorithm::class]);
    }
}
